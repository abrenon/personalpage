---
title: Alice BRENON's homepage
---

## Presentation

My name is Alice BRENON, I'm a Ph.D student working on the
[GÉODE](https://geode-project.github.io/) project, under the joint supervision
of [Denis Vigier](http://www.icar.cnrs.fr/membre/dvigier/), [Frédérique
Laforest](https://perso.liris.cnrs.fr/flaforest/) and [Ludovic
Moncla](https://ludovicmoncla.github.io/). I'm a member of both [ICAR (UMR
5191)](http://icar.cnrs.fr/) and [LIRIS (UMR 5205)](https://liris.cnrs.fr/)
laboratories.

I'm interested in functional programming, linguistics and pretty much
everything in between. My work in GÉODE will consist in devising methods and
tools to study if and how the geographical discourse in encyclopedias has
evolved from the Age of Enlightenment.

## Publications

I’ve contributed to the following papers and presentations:

- Moncla, L., Chabane, K., Brenon, A. (2022) https://editions-rnti.fr/?inprocid=1002717
- Brenon, A. & Vigier, D. (2021). *The specificities of encoding encyclopedias:
  towards a new standard ?*. 11th International Conference on Historical
  Lexicography and Lexicology. La Rioja, Spain.
- Vigier, D., Moncla, L., Brenon, A., Mcdonough, K., & Joliveau, T. (2020).
  *Classification des entités nommées dans l’Encyclopédie ou dictionnaire
  raisonné des sciences des arts et des métiers par une société de gens de
  lettres (1751-1772)*. 7e Congrès Mondial de Linguistique Française (CMLF).
  Montpellier, France.
- Moncla, L., McDonough, K., Vigier, D., Joliveau, T., & Brenon, A. (2019).
  *Toponym disambiguation in historical documents using network analysis of
  qualitative relationships*. Proceedings of the 3rd ACM SIGSPATIAL
  International Workshop on Geospatial Humanities, 1–4. Chicago, IL, USA

## Tools

I have developped and maintain the following tools:

- [chaoui](https://gitlab.huma-num.fr/alicebrenon/chaoui) : a web application
  to view ALTO files and handle blocklists of strings to delete, used in
  Disco-LGE to clean pages scanned from La Grande Encyclopédie. You can try it
  [here](chaoui)
- [soprano](https://gitlab.huma-num.fr/disco-lge/soprano) : a tool to process
  ALTO files containing the pages of an encyclopedia into a set of
  XML-TEI-encoded articles after fixing the layout.

## Teaching

I teach the [IST-4-JAV](IST-4-JAV.html) class at [INSA-Lyon](https://www.insa-lyon.fr/).
