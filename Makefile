SRC = index.md
TARGETS = $(SRC:%.md=%.html) chaoui/ EDdA-explorer/
HOST = connect.liris.cnrs.fr

all: $(TARGETS)

%.html: %.md
	pandoc -s $< -o $@

deploy: $(TARGETS)
	$(foreach TARGET,$(TARGETS),rsync -aHP $(TARGET) $(HOST):$(TARGET);)

mrproper:
	rm -f $(TARGET)
